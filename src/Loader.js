import React from 'react'
import loader from './sass/components/loader.module.scss'

const Loader = ({ color }) => (
  <div className={loader.ldsFacebook}>
    <div style={{ backgroundColor: color }}></div>
    <div style={{ backgroundColor: color }}></div>
    <div style={{ backgroundColor: color }}></div>
  </div>
);

export default Loader