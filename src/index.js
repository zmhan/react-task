import React from 'react'
import ReactDOM from 'react-dom'
import './sass/components/base.module.scss'
import Container from './Container'
ReactDOM.render(<Container />, document.getElementById('root'))
