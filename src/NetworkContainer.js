import React, { useState, useEffect } from "react";
import Favorities from './Favorities.js'
import network from "./sass/components/network.module.scss";
import card from "./sass/components/card.module.scss";
import footer from "./sass/components/footer.module.scss";


const NetworkContainer = ({ feed, firstSectionDis }) => {
  const [favorities, setFavorities] = useState([])
  const [isOn, setIsOn] = useState(false)

  useEffect(() => {
    const favoritiesJSON = sessionStorage.getItem('favorities')
    if (favoritiesJSON != null) setFavorities(JSON.parse(favoritiesJSON))
  }, [])

  useEffect(() => {
    sessionStorage.setItem('favorities', JSON.stringify(favorities))
  }, [favorities])


  const handleClick = (e) => {
    const btn = e.target.closest(`.${card.iconHeart}`)
    if (btn) {
      const Item = feed.stations.find(item => item.id === btn.dataset.id)


      if (btn.classList.contains(card.iconHeartActive)) {
        setFavorities([...favorities].filter(item => item.id !== Item.id))
        btn.classList.remove(card.iconHeartActive)
      } else {
        setFavorities([...favorities, Item])
        btn.classList.add(card.iconHeartActive)
      }
    }
  }

  const routeHandleOpen = () => {
    setIsOn(true)
  }

  const routeHandleClose = () => {
    setIsOn(false)
  }


  return (
    <>
      <header className={network.header}>
        <span className={network.title}>{feed.company}</span>
        <span className={network.count}>{feed.stations.length}</span>
      </header>
      <main className={network.listWrapper}>
        {
          isOn
          ?  <Favorities />
          :  feed.stations.map(item => {
              const inFavorite = favorities.find(f => f.id === item.id) ? card.iconHeartActive : ''
              return (
                <article key={item.id} className={card.card}>
                  <header>
                    <h2>{ item.name }</h2>
                  </header>
                  <div className={card.body}>
                    <p>{item.extra.address}</p>
                  </div>
                  <footer>
                  <span>{ new Date(item.timestamp).getFullYear() }</span>
                  <svg onClick={handleClick} data-id={item.id} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className={`${card.iconHeart} ${inFavorite}`}><circle cx="12" cy="12" r="10" className={card.primary}/><path className={card.secondary} d="M12.88 8.88a3 3 0 1 1 4.24 4.24l-4.41 4.42a1 1 0 0 1-1.42 0l-4.41-4.42a3 3 0 1 1 4.24-4.24l.88.88.88-.88z"/></svg>
                  </footer>
                </article>
              )
            })
        }
      </main>
      <footer className={footer.footer}>
      {
        isOn
          ? <div className={footer.favorites} onClick={routeHandleClose}>Show mainList</div>
          : <div className={footer.favorites} onClick={routeHandleOpen}>Show favorites</div>
      }
      </footer>
    </>
  );
};

export default NetworkContainer;
