import React, { useState, useEffect } from 'react';

import card from "./sass/components/card.module.scss";

const Favorities = () => {
  const [favorities, setFavorities] = useState([])

  useEffect(() => {
    const favoritiesJSON = sessionStorage.getItem('favorities')
    if (favoritiesJSON != null) setFavorities(JSON.parse(favoritiesJSON))
  }, [])

  useEffect(() => {
    sessionStorage.setItem('favorities', JSON.stringify(favorities))
  }, [favorities])

  const deleteItem = (e) => {
    const btn = e.target.closest(`.${card.iconHeart}`)
    if (btn) {
      setFavorities([...favorities].filter(item => item.id !== btn.dataset.id))
    }
  }

  return (
    <>
      {
        favorities.map(item => {
          return (
            <article key={item.id} className={card.card}>
              <header>
                <h2>{ item.name }</h2>
              </header>
              <div className={card.body}>
                <p>{item.extra.address}</p>
              </div>
              <footer>
               <span>{ new Date(item.timestamp).getFullYear() }</span>
               <svg onClick={deleteItem} data-id={item.id} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className={card.iconHeart + ' ' + card.iconHeartActive}><circle cx="12" cy="12" r="10" className={card.primary}/><path className={card.secondary} d="M12.88 8.88a3 3 0 1 1 4.24 4.24l-4.41 4.42a1 1 0 0 1-1.42 0l-4.41-4.42a3 3 0 1 1 4.24-4.24l.88.88.88-.88z"/></svg>
              </footer>
            </article>
          )
        })
      }
    </>
  )
}
export default Favorities