import React, { Fragment, useState, useEffect } from "react";
import { getCitybikFeeds, getCitybikFeedbyId } from './API'

import Section from "./Section.js";
import List from "./List.js";
import Loader from "./Loader.js";
import NetworkContainer from "./NetworkContainer";

const mapToStateFeeds = (feeds, id) =>
  feeds.map(feed => ({ ...feed, active: feed.id === id }));


const Container = () => {
  const [feeds, setFeeds] = useState(null);
  const [feed, setFeed] = useState(null);

  useEffect(() => {

    const fetchData = async () => {

      try {
        const id = await getCitybikFeeds(({ networks }) => {
          const firstId = networks[1].id
          setFeeds(mapToStateFeeds(networks, firstId))
          return firstId;
        })

        await getCitybikFeedbyId(id)(({ network }) => {
          setFeed(network)
        })

      } catch (error) {
        console.error(error)
      }
    }

    fetchData()

  }, []);

  const selectFeed = async idFeed => {
    await getCitybikFeedbyId(idFeed)(({ network }) => {
      setFeed(network)
      setFeeds(mapToStateFeeds(feeds, idFeed));
    })
  };

  return (
    <Fragment>
      <Section headingText="Feeds" className="feeds">
        {feeds ? (
          <List feeds={feeds} selectFeed={selectFeed}></List>
        ) : (
          <Loader color="white" />
        )}
      </Section>
      <Section className="items">
        {feed ? (
          <>
          <NetworkContainer
            feed={feed}
          />
          </>
        ) : (
          <Loader color="tomato" />
        )}
      </Section>
    </Fragment>
  );
};

export default Container;
