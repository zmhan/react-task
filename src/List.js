import React, { useState } from "react";
import list from "./sass/components/list.module.scss";
import button from "./sass/components/button.module.scss";

const RenderFeeds = ({ feeds, selectFeed, page, resPerPage }) => {
  const start = (page - 1) * resPerPage;
  const end = page * resPerPage;

  const handleClick = (e) => {
    const btn = e.target.closest("[data-url]")
    const activeFeeds = e.target.classList.contains(list.list__active)
    if (btn && !activeFeeds){
      selectFeed(btn.dataset.url)
    }
  }

  return feeds.slice(start, end).map(({ id, company, active }) => (
    <li key={id} onClick={handleClick} data-url={id}>
      <div
        className={`${list.list__center} ${active ? list.list__active : ""}`}
      >
        {company}
      </div>
    </li>
  ));
};

// type: 'prev' or 'next'
const Button = ({ page, type, goToPageHandler }) => { 
  const handleClick = (e) => {
    const btn = e.target.closest("#js-hook")
    if (btn){
      const goToPage = parseInt(btn.dataset.goto, 10)
      goToPageHandler(goToPage)
    }
  }
  return (
    <button
      id="js-hook"
      className={type === "prev" ? button.prev : button.next}
      data-goto={type === "prev" ? page - 1 : page + 1}
      onClick={handleClick}
    >
      {type}
      <span> page {type === "prev" ? page - 1 : page + 1}</span>
    </button>
  );
}

const ButtonBlock = ({ children }) => (
  <div className={button.block}>
    {children}
  </div>
);

const RenderButtons = ({ numResults, page, resPerPage, goToPageHandler }) => {
  const pages = Math.ceil(numResults / resPerPage);

  if (page === 1 && pages > 1) {
    // only button to go next page
    return (
      <ButtonBlock>
        <Button type="next" page={page} goToPageHandler={goToPageHandler}/>
      </ButtonBlock>
    )
  } else if (page < pages) {
    return (
      <ButtonBlock>
        <Button type="prev" page={page} goToPageHandler={goToPageHandler}/>
        <Button type="next" page={page} goToPageHandler={goToPageHandler}/>
      </ButtonBlock>
    )
  } else if (pages === page && pages > 1) {
    return (
      <ButtonBlock>
        <Button type="prev" page={page} goToPageHandler={goToPageHandler}/>
      </ButtonBlock>
    )
  }
};

const List = ({ feeds, selectFeed }) => {
  const [page, setPage] = useState(1);
  const resPerPage = 10;

  const goToPage = (num) => {
    setPage(num)
  }

  return (
    <>
      <ul className={list.list}>
        <RenderFeeds feeds={feeds} page={page} resPerPage={resPerPage} selectFeed={selectFeed}/>
      </ul>
      <RenderButtons numResults={feeds.length} page={page} resPerPage={resPerPage} goToPageHandler={goToPage} />
    </>
  );
};

export default List;