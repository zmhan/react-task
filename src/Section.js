import React from "react";
import section from "./sass/components/section.module.scss";

const Section = ({ children, className, headingText, headingLevel = 2 }) => {
  const H = `h${headingLevel}`;
  const sectionClassName =
    className === "feeds" ? section.feeds : section.items;

  return (
    <section className={sectionClassName}>
      {section.items === sectionClassName ? null : <H>{headingText}</H>}
      {children}
    </section>
  );
};

export default Section;
