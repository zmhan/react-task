import axios from "axios";

const getFromAPI = baseURL => endpoint => cb =>
  axios.get(`${baseURL}${endpoint}`)
    .then(res => cb(res.data))
    .catch(err => { console.error(err.message) })

const getCitybik = getFromAPI(
  'https://api.citybik.es/v2/networks'
)

export const getCitybikFeeds = getCitybik('?fields=id,company')
export const getCitybikFeedbyId = (id) => getCitybik(`/${id}`)
